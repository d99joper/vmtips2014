﻿using System;
using System.Data;
using System.Data.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMTips_2014.Models
{
    public class BlogRepository
    {
        private Tips_Entities db = new Tips_Entities();

        public void Save()
        {
            db.SaveChanges();
        }

        #region Blog

        public IQueryable<BlogEntry> GetAllBlogEntries()
        {
            return db.BlogEntries.OrderByDescending(b => b.ID);
        }

        public BlogEntry GetBlogEntry(int ID)
        {
            return db.BlogEntries.SingleOrDefault(b => b.ID == ID);
        }

        public BlogEntry GetLatestBlogEntry()
        {
            DateTime maxDate = (from b in db.BlogEntries
                                select b.PostedDate).Max();

            return GetAllBlogEntries().SingleOrDefault(b => b.PostedDate == maxDate);
        }


        public void Add(BlogEntry b)
        {
            db.BlogEntries.Add(b);
        }

        public void Delete(BlogEntry b)
        {
            db.BlogEntries.Remove(b);
        }

        #endregion


        #region Comments

        public IQueryable<Comment> GetAllComments()
        {
            return db.Comments.OrderByDescending(c => c.PostedDate);
        }

        public Comment GetComment(int commentID)
        {
            return db.Comments.SingleOrDefault(c => c.ID == commentID);
        }
        
        public void Add(Comment c)
        {
            db.Comments.Add(c);
        }

        public void Delete(Comment c)
        {
            db.Comments.Remove(c);
        }

        #endregion
    }
}
