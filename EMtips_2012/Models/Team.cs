//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VMTips_2014.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Team
    {
        public Team()
        {
            this.Match = new HashSet<Match>();
            this.Match1 = new HashSet<Match>();
            this.UserBronzeTeam = new HashSet<UserBronzeTeam>();
            this.UserFinalTeams = new HashSet<UserFinalTeam>();
            this.UserGoldTeam = new HashSet<UserGoldTeam>();
            this.UserPlayoffTeams = new HashSet<UserPlayoffTeam>();
            this.UserQFTeams_2014 = new HashSet<UserQFTeam>();
            this.UserSFTeams = new HashSet<UserSFTeam>();
            this.UserSilverTeam = new HashSet<UserSilverTeam>();
            this.TopScorer = new HashSet<TopScorer>();
        }
    
        public int ID { get; set; }
        public string GroupID { get; set; }
        public string TeamName { get; set; }
        public bool IsInPlayOffs { get; set; }
        public bool IsInQuarterFinals { get; set; }
        public bool IsInSemiFinals { get; set; }
        public bool IsInFinal { get; set; }
        public bool WonGold { get; set; }
        public bool WonSilver { get; set; }
        public bool WonBronze { get; set; }
        public Nullable<byte> PlayOffPos { get; set; }
    
        public virtual ICollection<Match> Match { get; set; }
        public virtual ICollection<Match> Match1 { get; set; }
        public virtual ICollection<UserBronzeTeam> UserBronzeTeam { get; set; }
        public virtual ICollection<UserFinalTeam> UserFinalTeams { get; set; }
        public virtual ICollection<UserGoldTeam> UserGoldTeam { get; set; }
        public virtual ICollection<UserPlayoffTeam> UserPlayoffTeams { get; set; }
        public virtual ICollection<UserQFTeam> UserQFTeams_2014 { get; set; }
        public virtual ICollection<UserSFTeam> UserSFTeams { get; set; }
        public virtual ICollection<UserSilverTeam> UserSilverTeam { get; set; }
        public virtual TeamStats TeamStats { get; set; }
        public virtual ICollection<TopScorer> TopScorer { get; set; }
    }
}
