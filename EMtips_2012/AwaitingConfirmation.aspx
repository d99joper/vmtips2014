﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AwaitingConfirmation.aspx.cs" Inherits="VMTips_2014.AwaitingConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

        <h4>
            Din kupong är ett steg från att bli skickad till Magnus.
        </h4>
        
        <h6>
            För att undvika spam, och att kuponger eventuellt skickas under falskt namn, krävs ännu ett steg för att din kupong ska registreras.            
        </h6>
    
        <h6>
            Ett mail har skickats till den e-postadress du angav.
        </h6>
        
        <h6>
            För att bekräfta din kupong, klicka helt enkelt på länken i mailet.
        </h6>
        
    
</asp:Content>
